import 'package:dartz/dartz.dart';
import 'package:seal_mobile_test/app/domain/entities/query_params.entity.dart';
import 'package:seal_mobile_test/app/domain/repositories/remote/iremote.repository.dart';

class GetConversionUsecase {
  final IRemoteRepository _remoteRepository;

  GetConversionUsecase(this._remoteRepository);

  Future<Either<bool, double>> call(QueryParams queryParams) async {
    var result = await _remoteRepository.getResult(queryParams);
    return result;
  }
}
