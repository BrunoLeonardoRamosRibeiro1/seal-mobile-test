import 'package:seal_mobile_test/app/data/models/query.model.dart';
import 'package:seal_mobile_test/app/domain/repositories/local/ilocal.repository.dart';

class GetQueryHistoryUsecase {
  final ILocalRepository _localRepository;

  GetQueryHistoryUsecase(this._localRepository);

  Future<List<QueryModel>> call() async {
    return await _localRepository.getQueryHistory();
  }
}
