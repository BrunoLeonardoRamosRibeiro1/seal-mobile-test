import 'package:seal_mobile_test/app/domain/repositories/local/ilocal.repository.dart';

class ClearQueryHistoryUsecase {
  final ILocalRepository _localRepository;

  ClearQueryHistoryUsecase(this._localRepository);

  Future call() async {
    return await _localRepository.clearQueryHistory();
  }
}