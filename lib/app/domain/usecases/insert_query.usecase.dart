import 'package:seal_mobile_test/app/data/models/query.model.dart';
import 'package:seal_mobile_test/app/domain/repositories/local/ilocal.repository.dart';

class InsertQueryUsecase {
  final ILocalRepository _localRepository;

  InsertQueryUsecase(this._localRepository);

  Future call(QueryModel queryModel) async {
    return _localRepository.insertQuery(queryModel);
  }
}
