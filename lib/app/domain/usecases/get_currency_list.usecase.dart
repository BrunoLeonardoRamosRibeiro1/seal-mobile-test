import 'package:seal_mobile_test/app/domain/repositories/local/ilocal.repository.dart';

class GetCurrencyListUsecase {
  final ILocalRepository _localRepository;

  GetCurrencyListUsecase(this._localRepository);

  List<String> call() {
    return _localRepository.getCurrencyList();
  }
}
