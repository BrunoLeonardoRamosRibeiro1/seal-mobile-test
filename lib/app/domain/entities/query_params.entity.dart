class QueryParams {
  String currency;
  String currencyToConvert;
  double value;

  QueryParams({required this.currency, required this.currencyToConvert, required this.value});
}