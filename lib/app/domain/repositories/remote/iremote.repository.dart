import 'package:dartz/dartz.dart';
import 'package:seal_mobile_test/app/domain/entities/query_params.entity.dart';

abstract class IRemoteRepository {
  Future<Either<bool, double>> getResult(QueryParams queryParams);
}