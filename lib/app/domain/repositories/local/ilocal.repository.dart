import 'package:seal_mobile_test/app/data/models/query.model.dart';

abstract class ILocalRepository {
  List<String> getCurrencyList();
  Future insertQuery(QueryModel queryModel);
  Future<List<QueryModel>> getQueryHistory();
  Future clearQueryHistory();
}
