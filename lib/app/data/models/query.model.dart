// To parse this JSON data, do
//
//     final queryModel = queryModelFromJson(jsonString);

import 'dart:convert';

QueryModel queryModelFromJson(String str) =>
    QueryModel.fromJson(json.decode(str));

String queryModelToJson(QueryModel data) => json.encode(data.toJson());

class QueryModel {
  QueryModel({
    required this.currency,
    required this.value,
    required this.currencyToConvert,
    required this.valueResult,
  });

  final String currency;
  final double value;
  final String currencyToConvert;
  final double valueResult;

  QueryModel copyWith({
    required String currency,
    required double value,
    required String currencyToConvert,
    required double valueResult,
  }) =>
      QueryModel(
        currency: currency,
        value: value,
        currencyToConvert: currencyToConvert,
        valueResult: valueResult,
      );

  factory QueryModel.fromJson(Map<String, dynamic> json) => QueryModel(
        currency: json["currency"],
        value: json["value"].toDouble(),
        currencyToConvert: json["currencyToConvert"],
        valueResult: json["valueResult"].toDouble(),
      );

  Map<String, dynamic> toJson() => {
        "currency": currency,
        "value": value,
        "currencyToConvert": currencyToConvert,
        "valueResult": valueResult,
      };
}
