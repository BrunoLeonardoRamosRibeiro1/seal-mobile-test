import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';

abstract class ISharedPref {
  read(String key);

  save(String key, value);

  remove(String key);

  Future<bool> checkIfLocalDataExists(String key);
}

class SharedPref implements ISharedPref {
  @override
  read(String key) async {
    final prefs = await SharedPreferences.getInstance();
    return json.decode(prefs.getString(key)!);
  }

  @override
  save(String key, value) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setString(key, json.encode(value));
  }

  @override
  remove(String key) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.remove(key);
  }

  @override
  Future<bool> checkIfLocalDataExists(String key) async {
    var prefs = await SharedPreferences.getInstance();

    if (prefs.getString(key) == null) {
      return false;
    }
    return true;
  }
}
