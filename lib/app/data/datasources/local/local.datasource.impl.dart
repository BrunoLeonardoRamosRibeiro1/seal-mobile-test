import 'package:seal_mobile_test/app/data/datasources/local/ilocal.datasource.dart';
import 'package:seal_mobile_test/app/data/models/query.model.dart';
import 'package:seal_mobile_test/app/data/services/shared_preferences.service.dart';

const QUERY = 'queryHistory';

class LocalDatasourceImpl implements ILocalDatasource {
  final ISharedPref _sharedPref;

  LocalDatasourceImpl(this._sharedPref);

  @override
  List<String> getCurrencyList() {
    return [
      'BRL',
      'USD',
      'EUR',
      'JPY',
      'CNY',
    ];
  }

  @override
  Future<List<QueryModel>> getQueryHistory() async {
    bool queryHistoryExists = await _sharedPref.checkIfLocalDataExists(QUERY);
    if (queryHistoryExists) {
      Iterable list = await _sharedPref.read(QUERY);
      return list.map((json) => QueryModel.fromJson(json)).toList();
    }
    return <QueryModel>[];
  }

  @override
  Future insertQuery({required QueryModel queryModel}) async {
    var listQueryHistoryExists =
        await _sharedPref.checkIfLocalDataExists(QUERY);

    List<QueryModel> listOfQueryModel;

    if (!listQueryHistoryExists) {
      listOfQueryModel = [];
      await _sharedPref.save(QUERY, listOfQueryModel);
    } else {
      listOfQueryModel = await getQueryHistory();
    }

    listOfQueryModel.add(queryModel);
    return await _sharedPref.save(QUERY, listOfQueryModel);
  }

  @override
  Future clearQueryHistory() async {
    return await _sharedPref.remove(QUERY);
  }
}
