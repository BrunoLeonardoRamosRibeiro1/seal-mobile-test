import 'package:seal_mobile_test/app/data/models/query.model.dart';

abstract class ILocalDatasource {
  List<String> getCurrencyList();
  Future insertQuery({required QueryModel queryModel});
  Future<List<QueryModel>> getQueryHistory();
  Future clearQueryHistory();
}
