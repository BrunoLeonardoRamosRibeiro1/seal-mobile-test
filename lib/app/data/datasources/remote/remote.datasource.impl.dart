import 'dart:convert';

import 'package:seal_mobile_test/app/data/datasources/remote/iremote.datasource.dart';
import 'package:seal_mobile_test/app/domain/entities/query_params.entity.dart';
import 'package:seal_mobile_test/app/presenter/resources/constants.dart';
import 'package:http/http.dart' as http;

class RemoteDatasourceImpl implements IRemoteDatasource {
  final http.Client client;

  RemoteDatasourceImpl(this.client);

  @override
  Future<double> query(QueryParams queryParams) async {
    if (queryParams.value == 0.0) {
      return Future.value(0.0);
    } else {
      var query = queryParams.currency + '_' + queryParams.currencyToConvert;
      var url =
          '${Constants.baseUrl}$query${Constants.compactUltra}${Constants.apiKey}';

      var response = await client.get(Uri.parse(url));

      Map<String, dynamic> result = json.decode(response.body);

      double valueResult = double.parse(result.values.first.toString());

      double conversionResult =
          double.parse((valueResult * queryParams.value).toStringAsFixed(2));

      return Future.value(conversionResult);
    }
  }
}
