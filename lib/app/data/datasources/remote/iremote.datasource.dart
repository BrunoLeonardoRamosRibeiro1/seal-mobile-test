import 'package:seal_mobile_test/app/domain/entities/query_params.entity.dart';

abstract class IRemoteDatasource {
  Future<double> query(QueryParams queryParams);
}
