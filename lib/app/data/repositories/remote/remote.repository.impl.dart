import 'package:dartz/dartz.dart';
import 'package:seal_mobile_test/app/data/datasources/remote/iremote.datasource.dart';
import 'package:seal_mobile_test/app/domain/entities/query_params.entity.dart';
import 'package:seal_mobile_test/app/domain/repositories/remote/iremote.repository.dart';

class RemoteRepositoryImpl implements IRemoteRepository {
  final IRemoteDatasource _remoteDatasource;

  RemoteRepositoryImpl(this._remoteDatasource);

  @override
  Future<Either<bool, double>> getResult(QueryParams queryParams) async {
    try {
      double result = await _remoteDatasource.query(queryParams);

      return Right(result);
    } catch (e) {
      return const Left(false);
    }
  }
}
