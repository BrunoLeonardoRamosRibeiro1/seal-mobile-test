import 'package:seal_mobile_test/app/data/datasources/local/ilocal.datasource.dart';
import 'package:seal_mobile_test/app/data/models/query.model.dart';
import 'package:seal_mobile_test/app/domain/repositories/local/ilocal.repository.dart';

class LocalRepositoryImpl implements ILocalRepository {
  final ILocalDatasource _localDatasource;

  LocalRepositoryImpl(this._localDatasource);

  @override
  List<String> getCurrencyList() {
    return _localDatasource.getCurrencyList();
  }

  @override
  Future<List<QueryModel>> getQueryHistory() async {
    return await _localDatasource.getQueryHistory();
  }

  @override
  Future insertQuery(QueryModel queryModel) async {
    return await _localDatasource.insertQuery(queryModel: queryModel);
  }

  @override
  Future clearQueryHistory() async {
    return await _localDatasource.clearQueryHistory();
  }
}
