import 'package:get/get.dart';
import 'package:seal_mobile_test/app/presenter/pages/home/home.page.dart';
import 'package:seal_mobile_test/app/presenter/pages/query_history/query_history.page.dart';

import 'routes.dart';

class AppPages {
  static List<GetPage> pages = [
    GetPage(
      name: Routes.ROUTE_HOME,
      page: () => HomePage(),
    ),
    GetPage(
      name: Routes.ROUTE_QUERY_HISTORY,
      page: () => QueryHistoryPage(),
      transition: Transition.zoom,
    ),
  ];
}