import 'package:flutter/material.dart';
import 'package:get/get.dart';

void showError({required String error, required String message}) {
  Get.snackbar(error, message,
      colorText: Colors.white,
      backgroundColor: Colors.red,
      snackPosition: SnackPosition.BOTTOM,
      icon: const Icon(
        Icons.dangerous,
        color: Colors.white,
      ));
}
