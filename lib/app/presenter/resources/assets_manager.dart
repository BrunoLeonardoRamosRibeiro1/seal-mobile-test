const String IMAGE_PATH = "assets/images";

class ImageAssets {
  static const String logo = "$IMAGE_PATH/exchange-icon.png";
  static const String logoWhite = "$IMAGE_PATH/exchange-white.png";
}
