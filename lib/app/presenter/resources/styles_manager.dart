import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:seal_mobile_test/app/presenter/resources/font_manager.dart';

TextStyle _getTextStyle(double fontSize, FontWeight fontWeight, Color color) {
  return GoogleFonts.roboto(
    textStyle: TextStyle(
      fontSize: fontSize,
      fontWeight: fontWeight,
      color: color,
    ),
  );
}

/// Regular Style
TextStyle getRegularStyle(
    {double fontSize = FontSize.s20, required Color color}) {
  return _getTextStyle(
    fontSize,
    FontWeightManager.regular,
    color,
  );
}

/// Bold Text Style
TextStyle getBoldStyle({double fontSize = FontSize.s16, required Color color}) {
  return _getTextStyle(
    fontSize,
    FontWeightManager.bold,
    color,
  );
}

/// SemiBold Text Style
TextStyle getSemiBoldStyle(
    {double fontSize = FontSize.s32, required Color color}) {
  return _getTextStyle(
    fontSize,
    FontWeightManager.semiBold,
    color,
  );
}
