import 'package:flutter/material.dart';
import 'package:seal_mobile_test/app/presenter/resources/color_manager.dart';
import 'package:seal_mobile_test/app/presenter/resources/styles_manager.dart';
import 'package:seal_mobile_test/app/presenter/resources/values_manager.dart';

ThemeData getApplicationTheme() {
  return ThemeData(
    /// Main Colors of the app
    primaryColor: ColorManager.primary,
    primaryColorLight: ColorManager.primaryOpacity70,
    primaryColorDark: ColorManager.darkPrimary,
    disabledColor: ColorManager.grey,

    /// Ripple color
    splashColor: ColorManager.primaryOpacity70,

    /// Will be used in case of disabled button for example
    colorScheme:
        ColorScheme.fromSwatch().copyWith(secondary: ColorManager.grey),

    /// Card view theme
    cardTheme: CardTheme(
      color: ColorManager.white,
      shadowColor: ColorManager.grey,
      elevation: AppSize.s4,
    ),

    /// App Bar theme
    appBarTheme: AppBarTheme(
      centerTitle: true,
      color: ColorManager.primary,
      elevation: AppSize.s4,
      shadowColor: ColorManager.primaryOpacity70,
      titleTextStyle: getSemiBoldStyle(
        color: ColorManager.white,
      ),
    ),

    /// Button theme
    buttonTheme: ButtonThemeData(
      shape: StadiumBorder(),
      disabledColor: ColorManager.grey,
      buttonColor: ColorManager.primary,
      splashColor: ColorManager.primaryOpacity70,
    ),

    /// Elevated button theme
    elevatedButtonTheme: ElevatedButtonThemeData(
      style: ElevatedButton.styleFrom(
        textStyle: getRegularStyle(color: ColorManager.white),
        primary: ColorManager.primary,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(
            AppSize.s12,
          ),
        ),
      ),
    ),

    /// Text theme
    textTheme: TextTheme(
      headline1: getRegularStyle(color: ColorManager.darkGrey),
      subtitle1: getRegularStyle(color: ColorManager.white),
      subtitle2: getRegularStyle(color: ColorManager.primary),
      caption: getRegularStyle(color: ColorManager.darkGrey),
      bodyText1: getRegularStyle(color: ColorManager.grey),
    ),

  );
}
