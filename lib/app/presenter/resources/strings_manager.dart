class AppStrings {
  static const String currencyConverter = "Conversor de Moedas";
  static const String queryHistory = "Histórico";
  static const String to = "para";
  static const String toConvert = "CONVERTER";
  static const String showQueryHistory = "VER HISTÓRICO";
  static const String enterTheValueForConversion =
      "Digite o valor para conversão";
  static const String clearHistory = "LIMPAR HISTÓRICO";
  static const String ops = "OPS!";
  static const String showError =
      'Ocorreu um erro ao chamar a API para conversão.\n' +
          'Tente novmente mais tarde.';
}
