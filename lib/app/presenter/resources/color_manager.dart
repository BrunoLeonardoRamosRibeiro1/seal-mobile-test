import 'package:flutter/material.dart';

class ColorManager {
  static Color primary = HexColor.fromHex("#336699");
  static Color darkGrey = HexColor.fromHex("#555555");
  static Color grey = HexColor.fromHex("#BFBFBF");
  static Color lightGrey = HexColor.fromHex("#9E9E9E");
  static Color primaryOpacity70 = HexColor.fromHex("#B3336699");

  /// New colors
  static Color darkPrimary = HexColor.fromHex("#264C73");
  static Color fadedOrange = HexColor.fromHex("#EE9440");
  static Color white = HexColor.fromHex("#FFFFFF");
  static Color white2 = HexColor.fromHex("#FEFEFE");
  static Color error = HexColor.fromHex("#e61f34"); // Red Color
}

extension HexColor on Color {
  static Color fromHex(String hexColorString) {
    hexColorString = hexColorString.replaceAll('#', '');
    if (hexColorString.length == 6) {
      hexColorString = "FF" + hexColorString; // 8 char with opacity 100%
    }
    return Color(int.parse(hexColorString, radix: 16));
  }
}
