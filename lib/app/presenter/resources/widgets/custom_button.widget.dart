import 'package:flutter/material.dart';
import 'package:seal_mobile_test/app/presenter/resources/color_manager.dart';
import 'package:seal_mobile_test/app/presenter/resources/values_manager.dart';

class CustomButtonWidget extends StatelessWidget {
  final Widget child;
  final Function()? onPressed;
  final Color? color;

  CustomButtonWidget(
      {required this.child, required this.onPressed, this.color});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: AppSize.s60,
      width: double.infinity,
      padding:
      const EdgeInsets.only(left: AppPadding.p28, right: AppPadding.p28),
      child: ElevatedButton(
        style: ButtonStyle(
          backgroundColor:
          MaterialStateProperty.all(color ?? ColorManager.fadedOrange),
          elevation: MaterialStateProperty.all(AppSize.s8),
        ),
        onPressed: onPressed,
        child: child,
      ),
    );
  }
}