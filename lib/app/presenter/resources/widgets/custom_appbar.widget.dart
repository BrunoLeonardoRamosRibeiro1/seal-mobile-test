import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:seal_mobile_test/app/presenter/resources/color_manager.dart';
import 'package:seal_mobile_test/app/presenter/resources/values_manager.dart';

class CustomAppBarWidget extends StatelessWidget implements PreferredSizeWidget {
  final String title;
  final bool centerTitle;

  CustomAppBarWidget(this.title, {this.centerTitle = false});

  @override
  Widget build(BuildContext context) {
    return AppBar(
      title: Text(title),
      centerTitle: centerTitle,
      elevation: AppSize.none,
      systemOverlayStyle: SystemUiOverlayStyle(
        statusBarColor: ColorManager.primary,
        statusBarBrightness: Brightness.light,
        statusBarIconBrightness: Brightness.light,
      ),
    );
  }

  @override
  Size get preferredSize => Size(double.maxFinite, 56.0);
}
