import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:seal_mobile_test/app/domain/usecases/clear_query_history.usecase.dart';
import 'package:seal_mobile_test/app/domain/usecases/get_query_history.usecase.dart';
import 'package:seal_mobile_test/app/presenter/pages/query_history/query_history.controller.dart';
import 'package:seal_mobile_test/app/presenter/pages/query_history/widgets/listview_query_history.widget.dart';
import 'package:seal_mobile_test/app/presenter/pages/query_history/widgets/query_history_bottom_sheet.widget.dart';
import 'package:seal_mobile_test/app/presenter/resources/color_manager.dart';
import 'package:seal_mobile_test/app/presenter/resources/strings_manager.dart';
import 'package:seal_mobile_test/app/presenter/resources/styles_manager.dart';
import 'package:seal_mobile_test/app/presenter/resources/values_manager.dart';
import 'package:seal_mobile_test/app/presenter/resources/widgets/custom_appbar.widget.dart';

class QueryHistoryPage extends StatelessWidget {
  final GetQueryHistoryUsecase _getQueryHistoryUsecase =
      Get.find<GetQueryHistoryUsecase>();

  final ClearQueryHistoryUsecase _clearQueryHistoryUsecase =
      Get.find<ClearQueryHistoryUsecase>();

  @override
  Widget build(BuildContext context) {
    return GetBuilder<QueryHistoryController>(
      init: QueryHistoryController(
        getQueryHistoryUsecase: _getQueryHistoryUsecase,
        clearQueryHistoryUsecase: _clearQueryHistoryUsecase,
      ),
      builder: (controller) {
        return Scaffold(
          backgroundColor: ColorManager.primary,
          appBar: CustomAppBarWidget(AppStrings.queryHistory),
          resizeToAvoidBottomInset: true,
          body: Obx(
            () => Visibility(
              visible: controller.queryList.isNotEmpty,
              replacement: Center(
                child: Text(
                  'Sem histórico de consultas!',
                  style: getRegularStyle(
                    color: ColorManager.white,
                  ),
                ),
              ),
              child: SafeArea(
                child: CustomScrollView(
                  slivers: <Widget>[
                    SliverToBoxAdapter(
                      child: Padding(
                        padding: const EdgeInsets.only(
                            left: AppPadding.p16, right: AppPadding.p16),
                        child: ListViewQueryHistory(controller),
                      ),
                    ),
                    SliverFillRemaining(
                      hasScrollBody: false,
                      child: IntrinsicHeight(
                        child: Column(
                          mainAxisSize: MainAxisSize.max,
                          children: <Widget>[
                            Spacer(),
                            QueryHistoryBottomSheetWidget(controller),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}
