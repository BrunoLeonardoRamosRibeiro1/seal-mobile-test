import 'package:get/get.dart';
import 'package:seal_mobile_test/app/data/models/query.model.dart';
import 'package:seal_mobile_test/app/domain/usecases/clear_query_history.usecase.dart';
import 'package:seal_mobile_test/app/domain/usecases/get_query_history.usecase.dart';

class QueryHistoryController extends GetxController {
  final GetQueryHistoryUsecase getQueryHistoryUsecase;
  final ClearQueryHistoryUsecase clearQueryHistoryUsecase;

  QueryHistoryController(
      {required this.getQueryHistoryUsecase,
      required this.clearQueryHistoryUsecase});

  /// Query History List
  final RxList<QueryModel> _queryList = <QueryModel>[].obs;

  List<QueryModel> get queryList => _queryList.value;

  set queryList(List<QueryModel> queryList) {
    _queryList.clear();
    _queryList.assignAll(queryList);
  }

  /// Load Query List from Local DB
  Future loadQueryList() async {
    var result = await getQueryHistoryUsecase();
    queryList = result;

  }

  /// Clear all History
  void clearHistory() async{
    await clearQueryHistoryUsecase();
    await loadQueryList();
  }

  @override
  void onInit() async {
    await loadQueryList();
    super.onInit();
  }
}
