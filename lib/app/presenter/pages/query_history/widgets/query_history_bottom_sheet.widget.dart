import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:seal_mobile_test/app/presenter/pages/query_history/query_history.controller.dart';
import 'package:seal_mobile_test/app/presenter/resources/color_manager.dart';
import 'package:seal_mobile_test/app/presenter/resources/strings_manager.dart';
import 'package:seal_mobile_test/app/presenter/resources/styles_manager.dart';
import 'package:seal_mobile_test/app/presenter/resources/values_manager.dart';
import 'package:seal_mobile_test/app/presenter/resources/widgets/custom_button.widget.dart';

class QueryHistoryBottomSheetWidget extends StatelessWidget {
  final QueryHistoryController controller;

  QueryHistoryBottomSheetWidget(this.controller);

  @override
  Widget build(BuildContext context) {
    return Obx(
      () => Visibility(
        visible: controller.queryList.isNotEmpty,
        child: Container(
          color: ColorManager.primary,
          padding: const EdgeInsets.only(
              bottom: AppPadding.p16, top: AppPadding.p16),
          child: CustomButtonWidget(
              child: Text(
                AppStrings.clearHistory,
                style: getBoldStyle(color: ColorManager.white),
              ),
              color: ColorManager.darkPrimary,
              onPressed: () {
                controller.clearHistory();
              }),
        ),
      ),
    );
  }
}
