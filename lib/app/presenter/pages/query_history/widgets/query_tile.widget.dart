import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:seal_mobile_test/app/data/models/query.model.dart';
import 'package:seal_mobile_test/app/presenter/resources/assets_manager.dart';
import 'package:seal_mobile_test/app/presenter/resources/color_manager.dart';
import 'package:seal_mobile_test/app/presenter/resources/styles_manager.dart';
import 'package:seal_mobile_test/app/presenter/resources/values_manager.dart';

class QueryTile extends StatelessWidget {
  final QueryModel item;

  QueryTile({required this.item});

  /// Format to Brazil Currency
  final money = NumberFormat("##,##0.00", "pt_BR");

  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: Image.asset(
        ImageAssets.logoWhite,
        color: ColorManager.white2,
      ),
      title: Padding(
        padding: const EdgeInsets.only(bottom: AppPadding.p18),
        child: Text(
          '${item.currency} ${money.format(item.value)}',
          style: getRegularStyle(color: ColorManager.white),
        ),
      ),
      subtitle: Text(
          '${item.currencyToConvert} ${money.format(item.valueResult)}',
          style: getRegularStyle(color: ColorManager.white)),
    );
  }
}