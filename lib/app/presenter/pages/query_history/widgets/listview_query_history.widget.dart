import 'package:flutter/material.dart';
import 'package:seal_mobile_test/app/presenter/pages/query_history/query_history.controller.dart';
import 'package:seal_mobile_test/app/presenter/pages/query_history/widgets/query_tile.widget.dart';

class ListViewQueryHistory extends StatelessWidget {
  final QueryHistoryController controller;

  ListViewQueryHistory(this.controller);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      shrinkWrap: true,
      physics: const ClampingScrollPhysics(),
      itemCount: controller.queryList.length,
      itemBuilder: (_, index) {
        var item = controller.queryList[index];
        return Column(
          children: [
            if (index == 0)
              const Divider(
                color: Colors.white,
              ),
            QueryTile(item: item),
            const Divider(
              color: Colors.white,
            ),
          ],
        );
      },
    );
  }
}
