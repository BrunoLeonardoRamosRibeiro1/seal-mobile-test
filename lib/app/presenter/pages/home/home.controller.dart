import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:seal_mobile_test/app/data/models/query.model.dart';
import 'package:seal_mobile_test/app/domain/entities/query_params.entity.dart';
import 'package:seal_mobile_test/app/domain/usecases/get_conversion.usecase.dart';
import 'package:seal_mobile_test/app/domain/usecases/get_currency_list.usecase.dart';
import 'package:seal_mobile_test/app/domain/usecases/insert_query.usecase.dart';
import 'package:seal_mobile_test/app/presenter/resources/strings_manager.dart';
import 'package:seal_mobile_test/app/presenter/resources/utils.dart';
import 'package:seal_mobile_test/app/routes/routes.dart';

class HomeController extends GetxController {
  final GetCurrencyListUsecase currencyListUsecase;
  final InsertQueryUsecase insertQueryUsecase;
  final GetConversionUsecase getConversionUsecase;

  HomeController({
    required this.insertQueryUsecase,
    required this.getConversionUsecase,
    required this.currencyListUsecase,
  });

  /// Format to Brazil Currency
  final money = NumberFormat("##,##0.00", "pt_BR");

  /// Loading
  RxBool isLoading = false.obs;

  /// Value Text Field Controller
  final Rx<TextEditingController> _valueTextController = TextEditingController().obs;

  TextEditingController get valueTextController => _valueTextController.value;

  List<String> currencyList = [];

  /// Value to convert
  final RxDouble _valueToConvert = 0.0.obs;

  double get valueToConvert => _valueToConvert.value;

  set valueToConvert(double value) {
    _valueToConvert.value = value;
  }

  /// Value Result after conversion
  final RxString _valueResultAfterConversion = '0,00'.obs;

  String get valueResultAfterConversion => _valueResultAfterConversion.value;

  set valueResultAfterConversion(String value) {
    _valueResultAfterConversion.value = value;
  }

  /// Initial coin/currency
  final RxString _initialItem = ''.obs;

  String get initialItem => _initialItem.value;

  set initialItem(String value) {
    _initialItem.value = value;
  }

  /// Initial coin/currency to convert
  final RxString _initialItemToConvert = ''.obs;

  String get initialItemToConvert => _initialItemToConvert.value;

  set initialItemToConvert(String value) {
    _initialItemToConvert.value = value;
  }

  /// Value after conversion
  final RxString _conversionCurrency = ''.obs;

  String get conversionCurrency => _conversionCurrency.value;

  set conversionCurrency(String value) {
    _conversionCurrency.value = value;
  }

  /// Load on create controller
  void loadOnInit() {
    var items = currencyListUsecase();
    currencyList.clear();
    currencyList.assignAll(items);

    initialItem = currencyList.first;
    initialItemToConvert = currencyList.last;

    _valueTextController.value = TextEditingController(text: '');
  }

  /// Insert Query
  void insertQuery({
    required QueryParams queryParams,
    required double valueResult,
  }) async {
    QueryModel item = QueryModel(
      currency: queryParams.currency,
      value: queryParams.value,
      currencyToConvert: queryParams.currencyToConvert,
      valueResult: valueResult,
    );
    await insertQueryUsecase(item);
  }

  /// Button Convert
  void onConvertButtonClick() async {
    isLoading(true);

    QueryParams _query = QueryParams(
      currency: initialItem,
      currencyToConvert: initialItemToConvert,
      value: valueToConvert,
    );

    var result = await getConversionUsecase(_query);

    result.fold((l) {
      showError(
        error: AppStrings.ops,
        message: AppStrings.showError,
      );

      isLoading(false);
    }, (valueResult) {
      insertQuery(
        queryParams: _query,
        valueResult: valueResult,
      );
      valueResultAfterConversion = money.format(valueResult);
      isLoading(false);
    });
  }

  /// Navigate to Query History
  void onQueryHistoryButtonClick() {
    Get.toNamed(Routes.ROUTE_QUERY_HISTORY);
  }

  @override
  void onInit() {
    loadOnInit();
    super.onInit();
  }

  @override
  void onClose() {
    valueTextController.dispose();
    super.onClose();
  }
}
