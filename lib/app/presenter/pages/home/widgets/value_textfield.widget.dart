import 'package:brasil_fields/brasil_fields.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:seal_mobile_test/app/presenter/pages/home/home.controller.dart';
import 'package:seal_mobile_test/app/presenter/resources/color_manager.dart';
import 'package:seal_mobile_test/app/presenter/resources/font_manager.dart';
import 'package:seal_mobile_test/app/presenter/resources/strings_manager.dart';
import 'package:seal_mobile_test/app/presenter/resources/styles_manager.dart';
import 'package:seal_mobile_test/app/presenter/resources/values_manager.dart';

class ValueTextFieldWidget extends StatelessWidget {
  final HomeController controller;

  ValueTextFieldWidget(this.controller);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: ColorManager.white,
      margin: const EdgeInsets.all(AppPadding.p28),
      child: Material(
        elevation: AppSize.s8,
        child: Padding(
          padding:
              const EdgeInsets.only(left: AppPadding.p8, right: AppPadding.p8),
          child: TextField(
            controller: controller.valueTextController,
            decoration: const InputDecoration(
              hintText: AppStrings.enterTheValueForConversion,
              border: InputBorder.none,
            ),
            keyboardType: TextInputType.number,
            inputFormatters: [
              FilteringTextInputFormatter.digitsOnly,
              CentavosInputFormatter(casasDecimais: 2),
            ],
            style: getRegularStyle(
                color: ColorManager.darkGrey, fontSize: FontSize.s20),
            textAlign: TextAlign.center,
            onChanged: (value) {
              controller.valueToConvert = 0.00;
              var parse = double.tryParse(
                  value.replaceAll('.', '').replaceAll(',', '.'));
              if (parse == null) {
                controller.valueToConvert = 0.00;
              } else {
                controller.valueToConvert = parse;
              }
            },
          ),
        ),
      ),
    );
  }
}
