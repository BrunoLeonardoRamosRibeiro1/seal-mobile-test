import 'package:flutter/material.dart';
import 'package:seal_mobile_test/app/presenter/pages/home/home.controller.dart';
import 'package:seal_mobile_test/app/presenter/pages/home/widgets/custom_combobox.widget.dart';
import 'package:seal_mobile_test/app/presenter/resources/color_manager.dart';
import 'package:seal_mobile_test/app/presenter/resources/font_manager.dart';
import 'package:seal_mobile_test/app/presenter/resources/strings_manager.dart';
import 'package:seal_mobile_test/app/presenter/resources/styles_manager.dart';

class RowComboBoxWidget extends StatelessWidget {
  final HomeController controller;

  RowComboBoxWidget(this.controller);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        CustomComboBoxWidget(controller: controller, toConvert: false),
        Text(
          AppStrings.to,
          style: getRegularStyle(
              fontSize: FontSize.s24, color: ColorManager.white),
        ),
        CustomComboBoxWidget(controller: controller, toConvert: true),
      ],
    );
  }
}
