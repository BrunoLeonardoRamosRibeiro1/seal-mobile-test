import 'package:flutter/material.dart';
import 'package:seal_mobile_test/app/presenter/resources/assets_manager.dart';
import 'package:seal_mobile_test/app/presenter/resources/values_manager.dart';

class LogoWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(
          vertical: AppPadding.p8, horizontal: AppPadding.p120),
      child: Image.asset(ImageAssets.logo),
    );
  }
}

