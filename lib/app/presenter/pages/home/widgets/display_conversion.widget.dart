import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:seal_mobile_test/app/presenter/pages/home/home.controller.dart';
import 'package:seal_mobile_test/app/presenter/resources/color_manager.dart';
import 'package:seal_mobile_test/app/presenter/resources/styles_manager.dart';
import 'package:seal_mobile_test/app/presenter/resources/values_manager.dart';

class DisplayConversionWidget extends StatelessWidget {
  final HomeController controller;

  DisplayConversionWidget(this.controller);

  @override
  Widget build(BuildContext context) {
    return Obx(
      () => Padding(
        padding: const EdgeInsets.only(left: AppPadding.p28),
        child: Align(
          alignment: Alignment.topLeft,
          child: Text(
            '\$ = ${controller.initialItemToConvert} ${controller.valueResultAfterConversion}',
            style: getRegularStyle(color: ColorManager.white2),
          ),
        ),
      ),
    );
  }
}
