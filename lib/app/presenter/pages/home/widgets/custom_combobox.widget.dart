import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:seal_mobile_test/app/presenter/pages/home/home.controller.dart';
import 'package:seal_mobile_test/app/presenter/resources/color_manager.dart';
import 'package:seal_mobile_test/app/presenter/resources/styles_manager.dart';
import 'package:seal_mobile_test/app/presenter/resources/values_manager.dart';

class CustomComboBoxWidget extends StatelessWidget {
  final bool toConvert;
  final HomeController controller;

  CustomComboBoxWidget({required this.controller, this.toConvert = false});

  @override
  Widget build(BuildContext context) {
    return Material(
      elevation: 5,
      child: Container(
        padding: const EdgeInsets.only(
          left: AppPadding.p8,
          right: AppPadding.p8,
        ),
        width: AppSize.s120,
        child: Obx(() {
          var items = controller.currencyList
              .map((item) => DropdownMenuItem<String>(
                    child: Text(item),
                    value: item,
                  ))
              .toList();

          return DropdownButtonHideUnderline(
            child: DropdownButton(
              value: toConvert ? controller.initialItemToConvert : controller.initialItem,
              style: getRegularStyle(color: ColorManager.grey),
              items: items,
              onChanged: (selected) {
                if (toConvert) {
                  controller.initialItemToConvert = selected as String;
                } else {
                  controller.initialItem = selected as String;
                }
              },
            ),
          );
        }),
      ),
    );
  }
}
