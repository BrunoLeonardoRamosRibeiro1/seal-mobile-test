import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:seal_mobile_test/app/domain/usecases/get_conversion.usecase.dart';
import 'package:seal_mobile_test/app/domain/usecases/get_currency_list.usecase.dart';
import 'package:seal_mobile_test/app/domain/usecases/insert_query.usecase.dart';
import 'package:seal_mobile_test/app/presenter/pages/home/home.controller.dart';
import 'package:seal_mobile_test/app/presenter/pages/home/widgets/display_conversion.widget.dart';
import 'package:seal_mobile_test/app/presenter/pages/home/widgets/logo.widget.dart';
import 'package:seal_mobile_test/app/presenter/pages/home/widgets/row_combobox.widget.dart';
import 'package:seal_mobile_test/app/presenter/pages/home/widgets/value_textfield.widget.dart';
import 'package:seal_mobile_test/app/presenter/resources/color_manager.dart';
import 'package:seal_mobile_test/app/presenter/resources/strings_manager.dart';
import 'package:seal_mobile_test/app/presenter/resources/styles_manager.dart';
import 'package:seal_mobile_test/app/presenter/resources/values_manager.dart';
import 'package:seal_mobile_test/app/presenter/resources/widgets/custom_appbar.widget.dart';
import 'package:seal_mobile_test/app/presenter/resources/widgets/custom_button.widget.dart';

class HomePage extends StatefulWidget {
  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final _getCurrencyListUsecase = Get.find<GetCurrencyListUsecase>();
  final _getConversionUsecase = Get.find<GetConversionUsecase>();
  final _insertQueryUsecase = Get.find<InsertQueryUsecase>();

  @override
  Widget build(BuildContext context) {
    return GetBuilder<HomeController>(
      init: HomeController(
        currencyListUsecase: _getCurrencyListUsecase,
        getConversionUsecase: _getConversionUsecase,
        insertQueryUsecase: _insertQueryUsecase,
      ),
      builder: (controller) {
        return Scaffold(
          backgroundColor: ColorManager.primary,
          appBar: CustomAppBarWidget(
            AppStrings.currencyConverter,
            centerTitle: true,
          ),
          body: SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                LogoWidget(),
                RowComboBoxWidget(controller),
                const SizedBox(height: AppSize.s16),
                ValueTextFieldWidget(controller),
                const SizedBox(height: AppSize.s28),
                DisplayConversionWidget(controller),
                const SizedBox(height: AppSize.s48),
                CustomButtonWidget(
                  child: Obx(
                    () => Visibility(
                      visible: controller.isLoading.isFalse,
                      replacement: const CircularProgressIndicator(),
                      child: Text(
                        AppStrings.toConvert,
                        style: getBoldStyle(color: ColorManager.white),
                      ),
                    ),
                  ),
                  onPressed: controller.isLoading.isFalse
                      ? () {
                          if (controller.valueToConvert != 0) {
                            controller.onConvertButtonClick();
                          }
                        }
                      : null,
                ),
                const SizedBox(height: AppSize.s16),
                CustomButtonWidget(
                  child: Text(
                    AppStrings.showQueryHistory,
                    style: getBoldStyle(color: ColorManager.white),
                  ),
                  color: ColorManager.grey,
                  onPressed: () {
                    controller.onQueryHistoryButtonClick();
                  },
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}
