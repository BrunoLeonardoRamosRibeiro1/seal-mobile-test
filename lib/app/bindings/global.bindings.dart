import 'package:get/get.dart';
import 'package:seal_mobile_test/app/data/datasources/local/local.datasource.impl.dart';
import 'package:seal_mobile_test/app/data/datasources/remote/remote.datasource.impl.dart';
import 'package:seal_mobile_test/app/data/repositories/local/local.repository.impl.dart';
import 'package:seal_mobile_test/app/data/repositories/remote/remote.repository.impl.dart';
import 'package:seal_mobile_test/app/data/services/shared_preferences.service.dart';
import 'package:seal_mobile_test/app/domain/usecases/clear_query_history.usecase.dart';
import 'package:seal_mobile_test/app/domain/usecases/get_conversion.usecase.dart';
import 'package:seal_mobile_test/app/domain/usecases/get_currency_list.usecase.dart';
import 'package:seal_mobile_test/app/domain/usecases/get_query_history.usecase.dart';
import 'package:seal_mobile_test/app/domain/usecases/insert_query.usecase.dart';
import 'package:http/http.dart' as http;

class GlobalBindings implements Bindings {
  @override
  void dependencies() {
    /// Client Http
    Get.lazyPut(() => http.Client());

    /// Shared Preferences
    Get.lazyPut(() => SharedPref());

    /// Remote Datasource
    final _httpClient = Get.find<http.Client>();
    Get.lazyPut(() => RemoteDatasourceImpl(_httpClient));

    /// Remote Repository
    final _remoteDatasource = Get.find<RemoteDatasourceImpl>();
    Get.lazyPut(() => RemoteRepositoryImpl(_remoteDatasource));

    /// Local Datasource
    final _sharedPref = Get.find<SharedPref>();
    Get.lazyPut(() => LocalDatasourceImpl(_sharedPref));

    /// Local Repository
    final _localDataSource = Get.find<LocalDatasourceImpl>();
    Get.lazyPut(() => LocalRepositoryImpl(_localDataSource));

    /// Local Use Cases
    final _localRepository = Get.find<LocalRepositoryImpl>();
    Get.put(GetCurrencyListUsecase(_localRepository), permanent: true);
    Get.put(GetQueryHistoryUsecase(_localRepository), permanent: true);
    Get.put(InsertQueryUsecase(_localRepository), permanent: true);
    Get.put(ClearQueryHistoryUsecase(_localRepository), permanent: true);

    /// Remote Use Case
    final _remoteRepository = Get.find<RemoteRepositoryImpl>();
    Get.put(GetConversionUsecase(_remoteRepository), permanent: true);
  }
}
