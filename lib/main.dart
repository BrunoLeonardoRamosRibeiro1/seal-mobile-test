import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:seal_mobile_test/app/bindings/global.bindings.dart';
import 'package:seal_mobile_test/app/presenter/resources/theme_manager.dart';
import 'package:seal_mobile_test/app/routes/pages.dart';
import 'package:seal_mobile_test/app/routes/routes.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    /// Portrait Screen
    SystemChrome.setPreferredOrientations(
        [DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]);

    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Seal Mobile Test',
      theme: getApplicationTheme(),
      initialBinding: GlobalBindings(),
      getPages: AppPages.pages,
      initialRoute: Routes.ROUTE_HOME,
    );
  }
}
