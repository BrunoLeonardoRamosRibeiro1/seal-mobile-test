## Seal Telecom

### Teste para vaga de desenvolvedor mobile em Flutter
Este repositório foi criado com o objetivo de testar os candidatos para a vaga de desenvolvedor mobile (Flutter).

### Instruções para rodar o projeto
1. A versão Flutter utilizada é a 2.8.1 (atualize, caso esteja rodando um anterior)
2. Faça o clone do projeto em **https://gitlab.com/BrunoLeonardoRamosRibeiro1/seal-mobile-test**
3. No terminal do VS Code ou Android Studio e digite **flutter clean && flutter pub get**
4. Para rodar o projeto digite [**flutter run**] na linha de comando

#### Instruções Básicas
1. Faça um fork deste repositório
2. Faça a implementação do layout seguindo o guideline que será informado abaixo.
4. Ideias para incrementar o teste serão bem vindas, sendo que os requisitos principais devem ser atingidos.
5. A api a ser utilizada no teste requer somente um e-mail para cadastro.
6. Após finalizar o teste, abra o arquivo README.md e escreva as intruções necessárias para instalar e executar sua entrega


#### Desafio (opcional para Junior): Conversor de Moedas em tempo real
1. Criar sua conta na api [Free Currency Converter API](https://free.currencyconverterapi.com/), para obter sua API_KEY.
2. Preencher as combos com a lista de moedas
    * BRL, USD, GBP, EUR, JPY, CNY
3. Preencher o valor no campo indicado no layout
4. Ao clicar em "Converter", a aplicação deve realizar:
    * Um request na api para obter a cotação do dia
    * Efetuar o cálculo de conversão com base na cotação e valor digitados.
    * Apesentar o resultado no campo indicado no layout.

###### Opcional para Pleno

5. Preencher a combo com a lista de moedas consumindo a rota existente na API.
6. Implementar Histórico de consultas
    * Armazenar as consultas
    * Limpar Histórico

#### O que esperamos no teste
* Um código Dart organizado com nomes de variáveis e funções auto explicativas
* Uma boa documentação para compilar o projeto corretamente para Andorid e iOS

#### Stack
* Dart/Flutter

#### Guideline (Layout)
Todo o layout está hospedado no Zeplin neste link: 
> https://scene.zeplin.io/project/61e6ba44040a3985f1215bcf

Icones podem ser baixados neste link: 
> https://storage.googleapis.com/sealtelecom/Others/Images/seal-mobile-test-icoins.zip

#### Envio do teste
1. Suba o repositório no seu Gitlab e envie o link com o assunto: **Teste para Dev Mobile** para Renato Bezerra [rbezerra@sealtelecom.com.br](mailto:rbezerra@sealtelecom.com.br)
